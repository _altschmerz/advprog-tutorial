package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;

public abstract class HighSpiritSpell implements Spell {
	protected HighSpirit spirit;

    public HighSpiritSpell(HighSpirit spirit) {
        // TODO: Complete Me
        this.spirit = spirit;
    }

    @Override
    public void undo() {
        // TODO: Complete Me
        HighSpiritState prevState = spirit.getPrevState();
        switch (prevState) {
            case ATTACK:
                spirit.attackStance();
                break;
            case DEFEND:
                spirit.defenseStance();
                break;
            case STEALTH:
                spirit.stealthStance();
                break;
            case SEALED:
                spirit.seal();
                break;
        }
    }
}
