package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    @Override
    public String defend() {
        return "Defend with barrier";
    }

    @Override
    public String getType() {
        return "barrier";
    }
}
