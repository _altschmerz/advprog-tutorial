package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me
    @Override
    public void update() {
        if (this.guild.getQuestType().equalsIgnoreCase("delivery") || this.guild.getQuestType().equalsIgnoreCase("escort")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
