package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me
    @Override
    public String defend() {
        return "Defend with shield";
    }

    @Override
    public String getType() {
        return "shield";
    }
}
