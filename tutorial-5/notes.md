# Key Requirements
- Menawarkan lowongan pada mata kuliah yang membutuhkan asisten
- Membuat mahasiswa dapat melamar pada lowongan yang tersedia
- Semua yang mendaftar ke suatu lowongan mata kuliah langsung diterima
- Mata kuliah yang sudah ada asisten tidak bisa dihapus
- Mahasiswa yang sudah menjadi asisten di suatu mata kuliah tidak bisa dihapus
- Setiap mahasiswa bekerja akan dicatat pada sebuah log
- Mahasiswa dapat membuat log
- Mahasiswa dapat memperbaharui log yang sudah dibuat
- Mahasiswa dapat menghapus log yang sudah dibuat
- Menyiapkan API untuk memperlihatkan memperlihatkan laporan pembayaran
- Mahasiswa dapat melihat laporan untuk bulan tertentu
- API mengembalikan sebuah laporan berisi semua pekerjaan asisten pada bulan tersebut dan summary jumlah uang yang didapatkan
- Pada laporan ada data bulan, jam kerja dengan satuan jam, pembayaran yang didapatkan dalam satuan Greil
- Bayaran 350 Greil per jam
- Sebuah log dapat berisikan data lebih singkat dari satu jam

# Relationship 
- Mahasiswa -> MataKuliah: ManyToOne
  - Mahasiswa hanya dapat menjadi asisten di satu mata kuliah
  - Satu mata kuliah dapat menerima banyak mahasiswa
- Mahasiswa -> Log: OneToMany
  - Satu mahasiswa dapat memiliki banyak log
  - Satu log pasti dimiliki oleh tepat satu mahasiswa

# Models
- Mahasiswa
  - npm: String (PK)
  - nama: String
  - email: String
  - ipk: String
  - noTelp: String
  - matkul: MataKuliah
  - listLog: List\<Log\>

- Mata Kuliah
  - kodeMatkul: String (PK)
  - namaMatkul: String
  - prodi: String
  - listAsdos: List\<Mahasiswa\>

- Log
  - idLog: int (PK)
  - waktuMulai: LocalDateTime
  - waktuBerakhir: LocalDateTime
  - deskripsi: String
  - mahasiswa: Mahasiswa

- Laporan Pembayaran
  - bulan: String
  - jamKerja: int
  - totalPembayaran: int
  - greilPerJam: int, final

# API
## Base URL
`http://localhost:8080`
URL specified below are concatenated from this base URL.

## Mahasiswa
- createMahasiswa
  
  `POST: /mahasiswa`

- getMahasiswa 

  `GET: /mahasiswa/{npm}`

- getListMahasiswa

  `GET: /mahasiswa`
  
- updateMahasiswa

  `PUT: /mahasiswa/{npm}`
  
- registerToMatkul

  `POST: /mahasiswa/register`
  
- deleteMahasiswa

  `DELETE: /mahasiswa/{npm}`
  
## Mata Kuliah
- createMataKuliah
  
  `POST: /mata-kuliah`

- getMataKuliah
  
  `GET: /mata-kuliah/{kodeMatkul}`

- getListMataKuliah
  
  `GET: /mata-kuliah`

- updateMataKuliah
  
  `PUT: /mata-kuliah/{kodeMatkul}`

- deleteMataKuliah
  
  `DELETE: /mata-kuliah/{kodeMatkul}`

## Log
- createLog
  
  `POST: /log/{npm}`

- getLog
  
  `GET: /log/{idLog}`

- getListLog
  
  `GET: /log`

- updateLog
  
  `PUT: /log/{idLog}`

- deleteLog
  
  `DELETE: /log/{idLog}`

## Laporan Pembayaran
- getLaporanPembayaranMahasiswa
  
  `GET: /laporan-pembayaran`
