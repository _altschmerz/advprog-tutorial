package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LaporanPembayaranServiceImplTest {
    @InjectMocks
    private LaporanPembayaranServiceImpl laporanPembayaranService;

    @Mock
    private LogService logService;

    private LaporanPembayaran laporanPembayaran;
    private Mahasiswa mahasiswa;
    private Log log;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906351013");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        log = new Log();
        log.setWaktuMulai(LocalDateTime.of(2021, 4, 5, 22, 00));
        log.setWaktuBerakhir(LocalDateTime.of(2021, 4, 5, 23, 30));
        log.setDeskripsi("Dummy description");
        log.setMahasiswa(mahasiswa);

        laporanPembayaran = new LaporanPembayaran("April", 1);
    }

    @Test
    void testServiceGetLaporanPembayaran() {
        lenient().when(logService.createLog("1906351013", log)).thenReturn(log);
        List<LaporanPembayaran> listLaporan = Arrays.asList(laporanPembayaran);
        lenient().when(laporanPembayaranService.getLaporanPembayaran("1906351013")).thenReturn(listLaporan);
//        List<LaporanPembayaran> resultLaporanPembayaran = laporanPembayaranService.getLaporanPembayaran("1906351013");
//        Assertions.assertEquals(listLaporan, resultLaporanPembayaran);
    }
}

