package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    @Mock
    private MahasiswaService mahasiswaService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906351013");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        log = new Log();
        log.setWaktuMulai(LocalDateTime.of(2021, 4, 5, 22, 00));
        log.setWaktuBerakhir(LocalDateTime.of(2021, 4, 5, 23, 30));
        log.setDeskripsi("Dummy description");
        log.setMahasiswa(mahasiswa);
    }

    @Test
    void testServiceGetLogById() {
        lenient().when(logService.getLog(log.getIdLog())).thenReturn(log);
        Log resultLog = logService.getLog(log.getIdLog());
        Assertions.assertEquals(log.getIdLog(), resultLog.getIdLog());
    }

    @Test
    public void testServiceGetListLog(){
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    void testServiceCreateLog() {
        lenient().when(logService.createLog("1906351013", log)).thenReturn(log);
        Log resultLog = logService.createLog("1906351013", log);
        Assertions.assertEquals(log.getIdLog(), resultLog.getIdLog());
    }

    @Test
    void testServiceUpdateLogById() {
        logService.createLog("1906351013", log);
        String currentDeskripsi = log.getDeskripsi();
        log.setDeskripsi("Another dummy description");

        lenient().when(logRepository.findByIdLog(log.getIdLog())).thenReturn(log);
        lenient().when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);
        Log resultLog = logService.updateLog(log.getIdLog(), log);

        assertNotEquals(resultLog.getDeskripsi(), currentDeskripsi);
        assertEquals(resultLog.getDeskripsi(), log.getDeskripsi());
    }

    @Test
    void testServiceDeleteLogById() {
        logService.createLog("1906351013", log);
        logService.deleteLog(log.getIdLog());
        Assertions.assertEquals(null, logService.getLog(log.getIdLog()));
    }
}

