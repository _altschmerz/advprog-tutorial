package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Log log;

    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp(){
        Mahasiswa mahasiswa = new Mahasiswa("1906351013", "Dummy", "dummy@gmail.com", "3.7", "081234567890");

        log = new Log("2021-04-05 22:00", "2021-04-05 23:30", "Dummy description", mahasiswa);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerPostLog() throws Exception{
        when(logService.createLog(anyString(), any(Log.class))).thenReturn(log);

        String postBody = "{\n" +
                "    \"waktuMulai\": \"2021-04-05 22:00\",\n" +
                "    \"waktuBerakhir\": \"2021-04-05 23:30\",\n" +
                "    \"deskripsi\": \"Dummy description\",\n" +
                "    \"npmMahasiswa\": \"1906351013\"\n" +
                "}";

        mvc.perform(post("/log/1906351013")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postBody))
                .andExpect(jsonPath("$.deskripsi").value("Dummy description"));
    }

    @Test
    public void testControllerGetLogById() throws Exception{
        when(logService.getLog(1)).thenReturn(log);

        mvc.perform(get("/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Dummy description"));
    }

    @Test
    public void testControllerGetNonExistLog() throws Exception{
        mvc.perform(get("/log/2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerUpdateLog() throws Exception{
        logService.createLog("1906351013", log);
        log.setDeskripsi("Another dummy description");
        when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);

        String putBody = "{\n" +
                "    \"waktuMulai\": \"2021-04-05 22:00\",\n" +
                "    \"waktuBerakhir\": \"2021-04-05 23:30\",\n" +
                "    \"deskripsi\": \"Another dummy description\",\n" +
                "    \"npmMahasiswa\": \"1906351013\"\n" +
                "}";

        mvc.perform(put("/log/" + log.getIdLog()).contentType(MediaType.APPLICATION_JSON)
                .content(putBody))
                .andExpect(status().isOk());
    }

    @Test
    public void testControllerDeleteLog() throws Exception{
        logService.createLog("1906351013", log);

        mvc.perform(delete("/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}
