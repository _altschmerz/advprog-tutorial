package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LaporanPembayaranServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LaporanPembayaranController.class)
public class LaporanPembayaranControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LaporanPembayaranServiceImpl laporanPembayaranService;

    private LaporanPembayaran laporanPembayaran;

    private Mahasiswa mahasiswa;

    private Log log;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("1906351013", "Dummy", "dummy@gmail.com", "3.7", "081234567890");

        laporanPembayaran = new LaporanPembayaran("April", 10);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGetLaporanPembayaranById() throws Exception{
        List<LaporanPembayaran> listLaporan = Arrays.asList(laporanPembayaran);
        when(laporanPembayaranService.getLaporanPembayaran("1906351013")).thenReturn(listLaporan);

        mvc.perform(get("/laporan-pembayaran/1906351013").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].jamKerja").value(10));
    }
}
