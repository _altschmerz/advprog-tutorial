package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface LogRepository extends JpaRepository<Log, String> {
    Log findByIdLog(int idLog);
    Iterable<Log> findByWaktuMulaiGreaterThanEqualAndWaktuBerakhirLessThanEqual(LocalDateTime waktuMulai, LocalDateTime waktuBerakhir);
}
