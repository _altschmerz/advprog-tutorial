package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name="log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @GeneratedValue
    @Column
    private int idLog;

    @Column
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime waktuMulai;

    @Column
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime waktuBerakhir;

    @Column
    private String deskripsi;

    @ManyToOne
    @JoinColumn(name="mahasiswa_id")
    private Mahasiswa mahasiswa;

    public Log(String waktuMulai, String waktuBerakhir, String deskripsi, Mahasiswa mahasiswa) {
        this.waktuMulai = LocalDateTime.parse(waktuMulai, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        this.waktuBerakhir = LocalDateTime.parse(waktuBerakhir, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        this.deskripsi = deskripsi;
        this.mahasiswa = mahasiswa;
    }

    public int getIdLog() {
        return idLog;
    }

    public LocalDateTime getWaktuMulai() {
        return waktuMulai;
    }

    public LocalDateTime getWaktuBerakhir() {
        return waktuBerakhir;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setIdLog(int idLog) {
        this.idLog = idLog;
    }

    public void setWaktuMulai(LocalDateTime waktuMulai) {
        this.waktuMulai = waktuMulai;
    }

    public void setWaktuBerakhir(LocalDateTime waktuBerakhir) {
        this.waktuBerakhir = waktuBerakhir;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }
}
