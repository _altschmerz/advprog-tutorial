package csui.advpro2021.tais.model;

public class LaporanPembayaran {
    private String bulan;
    private int jamKerja;
    private int totalPembayaran;
    private final int greilPerJam = 350;

    public LaporanPembayaran(String bulan, int jamKerja) {
        this.bulan = bulan;
        this.jamKerja = jamKerja;
    }

    public String getBulan() {
        return bulan;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public int getTotalPembayaran() {
        return totalPembayaran;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public void setTotalPembayaran() {
        this.totalPembayaran = jamKerja * greilPerJam;
    }
}
