package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "mata_kuliah")
@Data
@NoArgsConstructor
public class MataKuliah {
    @Id
    @Column(name = "kode_matkul", updatable = false)
    private String kodeMatkul;

    @Column(name = "nama_matkul")
    private String nama;

    @Column(name = "prodi")
    private String prodi;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "matkul")
    private List<Mahasiswa> listAsdos;

    public MataKuliah(String kodeMatkul, String nama, String prodi) {
        this.kodeMatkul = kodeMatkul;
        this.nama = nama;
        this.prodi = prodi;
    }

    public String getNama() {
        return nama;
    }

    public String getKodeMatkul() {
        return kodeMatkul;
    }

    public List<Mahasiswa> getListAsdos() {
        return this.listAsdos;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setKodeMatkul(String kodeMatkul) {
        this.kodeMatkul = kodeMatkul;
    }

    public void setProdi(String prodi) {
        this.prodi = prodi;
    }
}
