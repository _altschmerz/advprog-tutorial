package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log createLog(String npm, Log log) {
        log.setMahasiswa(mahasiswaService.getMahasiswaByNPM(npm));
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLog(int idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public Iterable<Log> getListLogByPeriode(LocalDateTime waktuMulai, LocalDateTime waktuBerakhir) {
        return logRepository.findByWaktuMulaiGreaterThanEqualAndWaktuBerakhirLessThanEqual(waktuMulai, waktuBerakhir);
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Log updateLog(int idLog, Log log) {
        log.setIdLog(idLog);
        Log prevLog = logRepository.findByIdLog(idLog);
        log.setMahasiswa(prevLog.getMahasiswa());
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLog(int idLog) {
        Log log = this.getLog(idLog);
        logRepository.delete(log);
    }
}
