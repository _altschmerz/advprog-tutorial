package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class LaporanPembayaranServiceImpl implements LaporanPembayaranService {
    @Autowired
    private LogService logService;

    @Override
    public List<LaporanPembayaran> getLaporanPembayaran(String npm) {
        List<LaporanPembayaran> listLaporan = new ArrayList<LaporanPembayaran>();
        for (int i=1; i<=12; i++) {
            LocalDate initialDate = LocalDate.of(2021, i, 1);
            LocalDate lastDate = initialDate.withDayOfMonth(initialDate.lengthOfMonth());
            LocalDateTime waktuMulai = LocalDateTime.of(initialDate, LocalTime.of(00, 00));
            LocalDateTime waktuBerakhir = LocalDateTime.of(lastDate, LocalTime.of(23, 59));
            Iterable<Log> logs = logService.getListLogByPeriode(waktuMulai, waktuBerakhir);

            int jamKerja = 0;
            for (Log log : logs) {
                LocalDateTime tempDateTime = LocalDateTime.from(log.getWaktuMulai());
                long jam = tempDateTime.until(log.getWaktuBerakhir(), ChronoUnit.HOURS);
                jamKerja += jam;
            }

            LaporanPembayaran laporanPembayaran = new LaporanPembayaran(new DateFormatSymbols().getMonths()[i-1], jamKerja);
            laporanPembayaran.setTotalPembayaran();
            listLaporan.add(laporanPembayaran);
        }

        return listLaporan;
    }
}
