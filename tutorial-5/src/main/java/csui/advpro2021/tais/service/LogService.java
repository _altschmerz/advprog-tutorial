package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;

import java.time.LocalDateTime;

public interface LogService {
    Log createLog(String npm, Log log);

    Log getLog(int idLog);

    Iterable<Log> getListLog();

    Iterable<Log> getListLogByPeriode(LocalDateTime waktuMulai, LocalDateTime waktuBerakhir);

    Log updateLog(int idLog, Log log);

    void deleteLog(int idLog);
}
