package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LaporanPembayaran;

import java.util.List;

public interface LaporanPembayaranService {
    List<LaporanPembayaran> getLaporanPembayaran(String npm);
}
