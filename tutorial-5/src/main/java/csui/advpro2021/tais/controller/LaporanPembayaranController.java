package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.service.LaporanPembayaranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "laporan-pembayaran")
public class LaporanPembayaranController {

    @Autowired
    private LaporanPembayaranService laporanPembayaranService;

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    public List<LaporanPembayaran> getLaporanPembayaranMahasiswa(@PathVariable String npm) {
        return laporanPembayaranService.getLaporanPembayaran(npm);
    }
}
