package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

import java.util.ArrayList;

public class Transformer {

    private AlphaCodex alphaCodex;
    private RunicCodex runicCodex;
    private ArrayList<Transformation> transformations;

    public Transformer() {
        this.alphaCodex = AlphaCodex.getInstance();
        this.runicCodex = RunicCodex.getInstance();

        transformations = new ArrayList<Transformation>();
        transformations.add(new AbyssalTransformation());
        transformations.add(new CelestialTransformation());
        transformations.add(new CaesarTransformation());
    }

    public String encode(String msg) {
        Spell spell = new Spell(msg, alphaCodex);
        for (int i=0; i<transformations.size(); i++) {
            spell = transformations.get(i).encode(spell);
        }
        spell = CodexTranslator.translate(spell, runicCodex);
        return spell.getText();
    }

    public String decode(String msg) {
        Spell spell = new Spell(msg, runicCodex);
        spell = CodexTranslator.translate(spell, alphaCodex);
        for (int i=0; i<transformations.size(); i++) {
            spell = transformations.get(i).decode(spell);
        }
        return spell.getText();
    }
}
