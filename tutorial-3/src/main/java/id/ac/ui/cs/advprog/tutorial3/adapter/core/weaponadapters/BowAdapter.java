package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import org.springframework.beans.factory.annotation.Autowired;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aimShotMode = false;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(aimShotMode);
    }

    @Override
    public String chargedAttack() {
        this.aimShotMode = !aimShotMode;
        return (aimShotMode ? "Entering" : "Leaving") + " aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
