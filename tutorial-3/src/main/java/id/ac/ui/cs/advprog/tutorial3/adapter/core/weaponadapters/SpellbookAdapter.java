package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isPrevLarge;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isPrevLarge = false;
    }

    @Override
    public String normalAttack() {
        isPrevLarge = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        String msg = (!isPrevLarge) ? spellbook.largeSpell() : "Can't cast large spells twice in a row!";
        isPrevLarge = true;
        return msg;
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
