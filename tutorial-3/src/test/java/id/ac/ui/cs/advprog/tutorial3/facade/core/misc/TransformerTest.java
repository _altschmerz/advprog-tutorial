package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class TransformerTest {

    private Class<?> TransformerClass;

    @BeforeEach
    public void setup() throws Exception {
        TransformerClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Transformer");
    }

    @Test
    public void testTransformerEncodeMethod() throws Exception {
        Class<?>[] encodeArgs = new Class[1];
        encodeArgs[0] = String.class;
        Method encodeMethod = TransformerClass.getDeclaredMethod("encode", encodeArgs);

        assertEquals("java.lang.String",
                encodeMethod.getGenericReturnType().getTypeName());
        assertEquals(1,
                encodeMethod.getParameterCount());
        assertTrue(Modifier.isPublic(encodeMethod.getModifiers()));
    }

    @Test
    public void testTransformerDecodeMethod() throws Exception {
        Class<?>[] decodeArgs = new Class[1];
        decodeArgs[0] = String.class;
        Method decodeMethod = TransformerClass.getDeclaredMethod("decode", decodeArgs);

        assertEquals("java.lang.String",
                decodeMethod.getGenericReturnType().getTypeName());
        assertEquals(1,
                decodeMethod.getParameterCount());
        assertTrue(Modifier.isPublic(decodeMethod.getModifiers()));
    }

    @Test
    public void testTransformerEncodeThenDecode() {
        String msg = "abcde";
        Transformer transformer = new Transformer();

        String encoded = transformer.encode(msg);
        String decoded = transformer.decode(encoded);

        assertEquals(msg, decoded);
    }
}
